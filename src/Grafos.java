

import java.util.Arrays;

public class Grafos {
	
	
	//matriz de adyacencia
	private int [][] matAdy;
	//matriz de adyacencia a la que le asignaremos 0 como peso dentro del constructor
	private int [][] matAdyPeso;
	//Vector de vertices el mismo estara lleno de nulos al crear el grafo
	private int [] vertsVec;
	
	
	//el contructor recibe como parametro la cantidad de vertices que va a contener el grafo
	//instancia una matriz de adyacencia de tipo entero, un vector de tipo vertice y le asigna 1...
	//for a toda la matriz de adyacencia,
	
	public Grafos(int cantidadMaximaDeVertices) {
		
		this.matAdyPeso = new int[cantidadMaximaDeVertices][cantidadMaximaDeVertices];
		this.vertsVec = new int[cantidadMaximaDeVertices];
		for(int i=0; i<vertsVec.length; i++)
			vertsVec[i] = 1;
		for(int i=0; i<cantidadMaximaDeVertices; i++) {
			for(int j=0; j<cantidadMaximaDeVertices; j++) {
				matAdyPeso[i][j] = 0;}}
	}
	
	//agregar peso o cambiar peso a una arista
	public void agregarPesoArista2(int vertice1, int vertice2, int peso) {
		this.matAdyPeso[vertice1][vertice2] = peso;
	}
	

	//crea una cadena que muestre los elementos del grafo
	@Override
	public String toString() {
		String cadena= "";
		int i=0;
		int j=0;
		
		for(i=0;i<matAdyPeso.length;i++) {
			cadena=cadena+ "[";
			for(j=0;j<matAdyPeso.length;i++) {
				cadena=cadena+matAdyPeso[i][j]+ ",";
			}
		}
		return cadena;
	}
	
	
//getters y setters
	public int[][] getMatAdyPeso() {
		return matAdyPeso;
	}

	public void setMatAdyPeso(int[][] matAdyPeso) {
		this.matAdyPeso = matAdyPeso;
	}

	public int[] getVertsVec() {
		return vertsVec;
	}

	public void setVertsVec(int[] vertsVec) {
		this.vertsVec = vertsVec;
	}
	
	public int getPeso(int fila,int columna) {
		
		return this.matAdyPeso[fila][columna];
	}
	
}