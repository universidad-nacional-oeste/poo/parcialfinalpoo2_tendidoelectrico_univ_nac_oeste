
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class PruebaStress {
	
	//este metodo crea un archivo de entrada para que lea el algoritmo de la clase tendidoelectrico y lograr el numero maximo de 
	//ciudades permitidas por el ejercicio y el numero minimo de centrales permitidas con el fin de estresarlo
	
		public static void crearArchivosDeEntrada() throws IOException {
			PrintWriter salida = new PrintWriter(new FileWriter("Archivos/ciudadesStress.in")); //creamos archivo
		   //declaramos variables
			int numero_centrales;
		    int ciudades_maxima=100;
		    int centrales_minimas=2;
		    
			salida.print(ciudades_maxima);//escribimos en el archivo las ciudades maximas=100
			salida.print(" ");// imprimimos un espacio
			salida.println(centrales_minimas);// imprimimos centrales minimas y salto de linea
			
			//imprimimos de forma aleatoria el numero de los vertices que son centrales
			for(int i=0; i<centrales_minimas; i++) {
				numero_centrales = (int) (Math.random() * ciudades_maxima) + 1;
				salida.print(numero_centrales);
			    salida.print(" ");
			}
			
			int i;
			int j;
			
			//instanciamos una matriz cuadrada con el numero de ciudades maximas(vertices)
			int matriz[][] = new int[ciudades_maxima][ciudades_maxima];
			//asignamos los pesos a la matriz.Para la diagonal i=j asigno 0. para los demas un numero random.
			for(i=0;i<matriz.length;i++) {
				
				for(j=0;j<matriz.length;j++) {
				if(i==j) {
					matriz[i][j]=0;
				}	else {
					int valor = (int) Math.floor(Math.random()*100+1);
					matriz[i][j]=valor;
					matriz[j][i]=valor;
				}
					
				}	}
			
			//imprimimos los valores de la matriz en el archivo
			for ( i = 0; i < matriz.length ; i ++) {
				salida.println();
				for ( j = 0; j < matriz.length; j ++) {
					salida.print(matriz[i][j] + " ");
				}
				
			}
			
			
			salida.close(); //cierro creacion archivo
			}
	
			
		
			
	
		//ejecucion programa probador. 
		
		public static void main(String[] args) throws IOException, NumberFormatException, RestriccionVioladaException {
			
			long TInicio, TFin, tiempo; //Variables para determinar el tiempo de ejecuci�n
			
			crearArchivosDeEntrada();//creamos archivo a leer y stresar el algoritmo
			
			
			
			TendidoElectrico tendido = new TendidoElectrico(); //instanciamos objeto tendido
			File archivo = new File("Archivos/ciudadesStress.in");//abrimos archivo para lectura
			Scanner sc = new Scanner(archivo);//iniciamos lectura
			int cantidad_vertices = sc.nextInt();//lee cantidad de vertices para iniciar el grafo
			sc.close();//cerramos archivo
			Grafos m0 = new Grafos(cantidad_vertices);
			m0.setMatAdyPeso(tendido.leerArchivo("Archivos/ciudadesStress.in"));///leemos la matriz de adyacencia desde el archivo creado
			TInicio = System.currentTimeMillis(); //Tomamos la hora en que inicio el algoritmo y la almacenamos en la variable inicio
			
			tendido.algoritmo(m0, tendido.centrales);//ejecutamos el algoritmo
			
			TFin = System.currentTimeMillis(); //Tomamos la hora en que finaliz� el algoritmo y la almacenamos en la variable T
			tiempo = TFin - TInicio; //Calculamos los milisegundos de diferencia
			System.out.println("Tiempo de ejecuci�n en milisegundos: " + tiempo); //Mostramos en pantalla el tiempo de ejecuci�n en milisegundos
			
		}

	}
	
