public class Arista {
	
	private int vertice1, vertice2, peso;//declaracion variables
	
	public Arista() {//constructor vacio de objeto arista
		
	}
	
	public Arista(int vertice1, int vertice2) {//constructor de objeto arista no ponderada con parametro int int origen destino
		this.vertice1 = vertice1;
		this.vertice2 = vertice2;
	}
	
	public Arista(int vertice1, int vertice2, int peso) { //constructor de objeto arista  ponderada con parametro int int origen destino peso
		this.vertice1 = vertice1;
		this.vertice2 = vertice2;
	}
	
	//getters y setters
	public int getVertice1() {
		return vertice1;
	}

	public void setVertice1(int vertice1) {
		this.vertice1 = vertice1;
	}

	public int getVertice2() {
		return vertice2;
	}

	public void setVertice2(int vertice2) {
		this.vertice2 = vertice2;
	}

	public int getPeso() {
		return peso;
	}

	public void setPeso(int peso) {
		this.peso = peso;
	}

	
}